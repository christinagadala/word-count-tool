# Word Count Tool

This Scala project provides a tool for analyzing word frequencies and their occurrences in a text document. It reads a plain text document from a URL, processes the content, and displays word frequencies in alphabetical order along with the line numbers they appear on.

## Files

The following files are located in the `src/main/scala` directory of this project:

- `Main.scala`: Contains the main entry point of the application. It retrieves a text file from a specified URL and calls the `frequencyByWord` function to compute and display word frequencies.

- `WordCount.scala`: Contains the implementation of the `WordCount` object, which provides the functionality to process a text document and calculate word frequencies.

## Usage

1. Clone this repository to your local machine.

2. Open a terminal and navigate to the project directory.

3. Run the application using the following command:

   ```bash
   sbt run
