import WordCount.frequencyByWord


object Main extends App {

  val file = "https://www.gutenberg.org/files/84/84-0.txt"

  // sort by words in alphabetical ascending order
  frequencyByWord(file).toSeq.sortBy { case(word,(count,lines)) =>
    word
  }.foreach(println)
}
