import scala.io.Source

object WordCount {

  // type aliases for readability
  type Word = String
  type Count = Int
  type LineNumber = Int

  /** Iterate through a plain text document from the specified url,
    * line by line. Split the line into words, calling frequencyByWord
    * at each iteration to compute the results from the line, and
    * finally merge it into the overall results from previous lines.
    *
    * @param  url the url to search
    * @return a map indexed by word with values representing their
    *         count and line numbers in which they occurred
    */
  def frequencyByWord(url:String): Map[Word,(Count,List[LineNumber])] = {
    Source.fromURL(url)
      .getLines
      .zipWithIndex
      .foldLeft(
        // initialize with an empty map
        Map.empty[Word,(Count,List[LineNumber])]
      ) { (resultSoFar, kv) =>
        val (line, lineNumber) = kv
        val wordsInLine = line
          // normalize all words
          .toLowerCase
          // parse out special characters and make them whitespace
          .replaceAll("[^a-zA-Z]"," ")
          // split at whitespace
          .split("\\s+")
          // remove empty lines
          .filterNot(_ == "")
        // merge map of prior lines w current one (will be computed from frequencyByWord)
        resultSoFar ++ frequencyByWord(resultSoFar, wordsInLine, lineNumber)
      }
  }

  /** Iterate through the words passed in, checking if the word is
    * found in the result so far. If it is, increment the count and
    * add the line number to the word's list of line numbers. If
    * the word is not found, add a new entry to the map with the word
    * as the key, the count as one, and the line number as the single
    * value in the word's list of line numbers.
    *
    * @param  resultSoFar the map of word frequencies computed
    *                     from previous lines
    * @param  wordsInLine the list of words from the current line
    * @param  lineNumber  the line number in which word occured
    * @return a map indexed by word with values representing their
    *         count and line numbers in which they occurred
    */
  private def frequencyByWord(
   resultSoFar: Map[Word,(Count,List[LineNumber])],
   wordsInLine: Seq[Word],
   lineNumber:Int
  ): Map[Word,(Count,List[LineNumber])] = {
    wordsInLine.foldLeft(
      // initialized the map with the result from previous lines
      resultSoFar
      // res is the result so far; word is the current word
    ){ case (res, word) =>
      res.get(word) match {
        // the word is found in results so far
        case Some(frequency) =>
          // extract the tuple from the word's value
          val (count, lineNums) = frequency
          // increment the count and insert line number, merge it
          res ++ Map(word -> (count + 1, lineNumber :: lineNums))
        // the word is not found
        case None =>
          // add new entry to map with count as 1 and the line number
          res ++ Map(word -> (1, List(lineNumber)))
      }
    }
  }
}
